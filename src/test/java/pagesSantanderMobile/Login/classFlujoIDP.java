package pagesSantanderMobile.Login;

import java.io.FileOutputStream;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import testSantanderMobile.DriverTest;
import testSantanderMobile.TestLogin;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import ElementsSantaderMobile.ElementsIDP;
import ElementsSantaderMobile.ElementsLogin;
import ElementsSantaderMobile.Rutas;
import EventosSantanderMobile.Eventos;
import helpers.Helpers;
import helpers.PdfGeneration;
import helpers.ScreenShots;
import items.Fecha;
import items.Fuentes;

public class classFlujoIDP {
	Helpers helper = new Helpers();
	ElementsLogin elements = new ElementsLogin();
	PdfGeneration pdf = new PdfGeneration();
	ScreenShots captura = new ScreenShots();
	Document document = new Document();
	java.util.Date fecha = new Date();
	Rutas rutas = new Rutas();
	Fecha nameFecha = new Fecha();
	Fuentes estilos = new Fuentes();
	DriverTest testDriver = new DriverTest();
	Eventos eventos = new Eventos();
	
	ElementsIDP idpe = new ElementsIDP();
	
	
	
	private WebDriver driver;
	
	private By boxCodCliente;
	private By btnContinuar;
	private By boxPassword;
	private By btnPassword;
	private By atencion;

	
	public classFlujoIDP(WebDriver driver){
		this.driver = driver;

		
		boxCodCliente = By.id(idpe.BOX_CODCL);
		btnContinuar = By.id(idpe.BTN_CONTINUAR);
		boxPassword = By.id(idpe.BOX_PASSWORD);
		btnPassword = By.id(idpe.BTN_PASSWORD);
		atencion = By.className(idpe.ATENCION);
		

	}
	

	public void classFlujoIDP(String numCliente, String password) throws Exception {
		try {
			pdf.instancia();
			pdf.open();
			pdf.parrafo("LISTA DE INSUMOS");
			
			testDriver.inyecta(driver);
			driver.findElement(boxCodCliente).sendKeys(numCliente);
			System.out.println("inyecta: " + numCliente);
			helper.sleepSeconds(5);
			driver.findElement(btnContinuar).click();
			helper.sleepSeconds(5);
			driver.findElement(boxPassword).sendKeys(password);
			System.out.println("inyecta: " + password);
			helper.sleepSeconds(5);
			driver.findElement(btnPassword).click();
			helper.sleepSeconds(5);
			
			WebElement a;
			a = driver.findElement(atencion);
			
			if (a != null){
				pdf.parrafo(numCliente + " " + "Incorrecto Sesion Abierta");
				System.out.println("Incorrecto Sesion Abierta");
			}
			
			pdf.parrafo(numCliente + " " + "OK");
			pdf.close();
		
		}catch(Exception e){
			
			pdf.parrafo(numCliente + " " + "Incorrecto Sesion Abierta");
			System.out.println("Fallo en el insumo");
			pdf.close();
		}
		
		
	}
	

	
}
