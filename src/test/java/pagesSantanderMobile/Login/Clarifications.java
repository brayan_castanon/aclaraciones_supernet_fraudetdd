package pagesSantanderMobile.Login;

import java.io.FileOutputStream;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import testSantanderMobile.DriverTest;
import testSantanderMobile.TestLogin;
import validateElementsClarification.ElementsMovements;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import ElementsSantaderMobile.ElementsClarification;
import ElementsSantaderMobile.Rutas;
import EventosSantanderMobile.Eventos;
import helpers.AgregarIMG;
import helpers.Helpers;
import helpers.PdfGeneration;
import helpers.ScreenShots;
import items.Fecha;
import items.Fuentes;

public class Clarifications {
	
	Helpers helper = new Helpers();
	ElementsClarification elements = new ElementsClarification();
	PdfGeneration pdf = new PdfGeneration();
	ScreenShots captura = new ScreenShots();	
	Document document = new Document();
	java.util.Date fecha = new Date();
	Rutas rutas = new Rutas();
	Fecha nameFecha = new Fecha();
	Fuentes estilos = new Fuentes();
	DriverTest testDriver = new DriverTest();
	Eventos eventos = new Eventos();
	AgregarIMG agregar = new AgregarIMG();
	ElementsMovements mov = new ElementsMovements();

	private WebDriver driver;
	
	public Clarifications(WebDriver driver) {
		this.driver = driver;
	}
	
	
	public void clarificationsMov () throws Exception {  

		JavascriptExecutor js = (JavascriptExecutor)driver;  
		testDriver.inyecta(driver);

		System.out.println("Aqui comienza el flujo.");
		
		pdf.instancia();
		pdf.open();
		pdf.agregaImg(pdf.encabezado());
		pdf.add(driver);
		
		
		eventos.clicId(eventos, elements.BTN_VERMAS, driver);
		pdf.add(driver);
		eventos.clicId(eventos, elements.MOVE, driver);
		pdf.add(driver);
	
		eventos.clicId(eventos, elements.CONTINUAR_A, driver);
		
		
		
		eventos.clicCss(eventos, elements.YES, driver);
		pdf.add(driver);
		eventos.clicCss(eventos, elements.NOT, driver);
		pdf.add(driver);
		eventos.clicCss(eventos, elements.NATIONAL, driver);
		pdf.add(driver);
		eventos.clicCss(eventos, elements.STATE, driver);
		pdf.add(driver);
		eventos.clicId(eventos, elements.CONTINUAR_Q, driver);
		pdf.add(driver);
		
		
		eventos.clicId(eventos, elements.CONFIRMAR_SUMARY, driver);
		pdf.add(driver);
		helper.sleepSeconds(1);
		eventos.clicId(eventos, elements.COFIRMAR_MODAL, driver);
		pdf.add(driver);
		helper.sleepSeconds(2);
		
		
		eventos.clicCss(eventos, elements.RADIOINM, driver);
		pdf.add(driver);
		eventos.clicId(eventos, elements.CONTINUAR_SUMMARY2, driver);
		System.out.println("Aqui termina el flujo.");
		helper.sleepSeconds(2);
		pdf.add(driver);
		
		pdf.close();

	}
	
	


}
