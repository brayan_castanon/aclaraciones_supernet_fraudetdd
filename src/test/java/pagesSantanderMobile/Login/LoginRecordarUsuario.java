package pagesSantanderMobile.Login;

import java.io.FileOutputStream;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import testSantanderMobile.DriverTest;
import testSantanderMobile.TestLogin;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import ElementsSantaderMobile.ElementsLogin;
import ElementsSantaderMobile.Rutas;
import EventosSantanderMobile.Eventos;
import helpers.Helpers;
import helpers.PdfGeneration;
import helpers.ScreenShots;
import items.Fecha;
import items.Fuentes;

public class LoginRecordarUsuario {
	
	Helpers helper = new Helpers();
	ElementsLogin elements = new ElementsLogin();
	PdfGeneration pdf = new PdfGeneration();
	ScreenShots captura = new ScreenShots();	
	Document document = new Document();
	java.util.Date fecha = new Date();
	Rutas rutas = new Rutas();
	Fecha nameFecha = new Fecha();
	Fuentes estilos = new Fuentes();
	DriverTest testDriver = new DriverTest();
	Eventos eventos = new Eventos();

	private WebDriver driver;
	
	public LoginRecordarUsuario(WebDriver driver) {
		this.driver = driver;
	}
	
	public void loginDos (String numCliente, String passwordI, String frase, String respuestab) throws Exception {  

		JavascriptExecutor js = (JavascriptExecutor)driver;  
		testDriver.inyecta(driver);
		
//		pdf.instancia();
//		pdf.open();
//		pdf.agregaImg(pdf.encabezado());
//		pdf.parrafo("holap");
//		pdf.status();
//		pdf.nuevaPagina();
//		pdf.parrafo("holap");
//		pdf.agregaImg(pdf.imageEvidencia("ingresarRecordar"));
//		pdf.close();
		
		//inicia cambio
		pdf.instancia();
		pdf.open();
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("LOGIN: RECORDAR USUARIO");
		pdf.parrafo("**Ingresar los datos de enrolamiento exitosamente y pasar a la pantalla de configuracion exitosa, La cual permitir� continuar con el proceso de registro del cliente.");
		pdf.nuevaPagina();
		
		try {
		
		pdf.agregaImg(pdf.encabezado());
		driver.navigate().refresh();
		helper.sleepSeconds(2);
		captura.takeScreenShotTest(driver, "ingresarRecordar");
		eventos.clicXpath(eventos, elements.INGRESAR_RECORDAR, driver);
		pdf.parrafo("Paso 1: Abrir super movil.");
		pdf.parrafo("Paso 2: Dar clic en el boton Ingresar");
		pdf.agregaImg(pdf.imageEvidencia("ingresarRecordar"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);

		eventos.textId(eventos, elements.PASSWORD, passwordI, driver);
		captura.takeScreenShotTest(driver, "passwordText");
		pdf.parrafo("Igresar Contrase�a de Usuario.");
		pdf.agregaImg(pdf.imageEvidencia("passwordText"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);
		
		
		eventos.clicXpath(eventos, elements.BTN_ING, driver);
		js.executeScript("document.body.style.zoom='.8'");
		captura.takeScreenShotTest(driver, "ingButton");
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("Mostrar la pantalla de Mis productos");
		pdf.agregaImg(pdf.imageEvidencia("ingButton"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);	
		

		pdf.nuevaPagina();
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("Mostrar pantalla de 'MIS PRODUCTOS'");
		pdf.parrafo("Muestra Saldos y Movimientos");
//		testDriver.scoll(driver);
		captura.takeScreenShotTest(driver, "finc");
		pdf.agregaImg(pdf.imageEvidencia("finc"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);
		

		
		}catch (Exception  ex){
			System.out.println("Existe algun error!! ojo!");
			System.out.println(ex);
			pdf.parrafo("Existe algun error!!");
		}
		
		pdf.close();
	}
	
	


}
