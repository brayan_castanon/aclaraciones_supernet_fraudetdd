package pagesSantanderMobile.Login;

import java.io.FileOutputStream;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.html5.LocalStorage;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import ElementsSantaderMobile.ElementsLogin;
import ElementsSantaderMobile.Rutas;
import helpers.Helpers;
import helpers.PdfGeneration;
import helpers.ScreenShots;
import items.Fecha;
import items.Fuentes;

public class LoginRSA {
	
	Helpers helper = new Helpers();
	ElementsLogin elements = new ElementsLogin();
	PdfGeneration pdf = new PdfGeneration();
	ScreenShots captura = new ScreenShots();
	Document document = new Document();
	java.util.Date fecha = new Date();
	Rutas rutas = new Rutas();
	Fecha nameFecha = new Fecha();
	Fuentes estilos = new Fuentes();

	private WebDriver driver;
	private By loginButton;
	private By loginText;
	private By ingresarButton;
	private By passwordText;
    private By slideButton;
	private By ingButton;
	private By imgSlideButton;
	private By fraseText;
	private By selecButton;
	private By selectPregunta;
	private By continuarButton;
	private By respuesta;
	private By slideRecordar;
	private By continuarb;
	private By continuarf;
	private By detallet;
	private By clicn;

	public LoginRSA(WebDriver driver) {
		this.driver = driver;
		loginButton = By.xpath(elements.BTN_INICIO);
		loginText   = By.id(elements.INPUT_CCLIENTE);
		ingresarButton = By.xpath(elements.BTN_INGRESAR);
		passwordText = By.id(elements.PASSWORD);
		slideButton = By.id(elements.BTN_SLIDE);
		ingButton = By.xpath(elements.BTN_ING);
		imgSlideButton = By.xpath(elements.BTN_SLIDE_IMG);
		fraseText = By.id(elements.FRASE_BIENV);
		selecButton = By.xpath(elements.PREGUNTA_SECRETA);
		selectPregunta = By.xpath(elements.SELEC_SECRETA);
		continuarButton = By.className(elements.CONTINUAR);
		respuesta = By.xpath(elements.RESPUESTA);
		slideRecordar = By.id(elements.SLIDE_RECORDAR);
		continuarb = By.xpath(elements.CONTINUAR_B);
		continuarf = By.xpath(elements.CONTINUAR_F);
		detallet = By.xpath(elements.DETALLE_T);
		clicn = By.xpath(elements.CLICK_NAV);
	}


	public void loginUno(String numCliente, String passwordI, String frase, String respuestab) throws Exception {    			
		Font  fuente = estilos.styles();
		String File = rutas.EVIDENCIAS + nameFecha.name() +".pdf";
		PdfWriter.getInstance(document, new FileOutputStream(File));
		JavascriptExecutor js = (JavascriptExecutor)driver; 
	
		
		try  {
		document.open();
		document.add(pdf.encabezado());
		document.add(new Paragraph("LOGIN: ENROLAMIENTO RSA PREAFILIADO", fuente ));
		document.add(new Paragraph("Ingresar los datos de enrolamiento exitosamente y pasar a la pantalla de configuracion exitosa, La cual permitir� continuar con el proceso de registro del cliente."));
		document.newPage();

		document.add(pdf.encabezado());
		captura.takeScreenShotTest(driver, "loginButton");
		driver.findElement(loginButton).click();
		document.add(new Paragraph("Paso 1: Abrir super movil.", fuente));
		document.add(new Paragraph("Paso 2: Dar clic en el boton Ingresar", fuente));
		Image loginButton = Image.getInstance("screen\\loginButton.png");
		loginButton.scaleAbsoluteWidth(260f);
		loginButton.scaleAbsoluteHeight(540f);
		loginButton.setAlignment(Element.ALIGN_CENTER);
		document.add(loginButton);
		document.newPage();
		helper.sleepSeconds(2);
			
		System.out.println(numCliente);
		document.add(pdf.encabezado());
		captura.takeScreenShotTest(driver, "loginTextPre");
		Image loginTextPre = Image.getInstance("screen\\loginTextPre.png");
		loginTextPre.scaleAbsoluteWidth(260f);
		loginTextPre.scaleAbsoluteHeight(540f);
		loginTextPre.setAlignment(Element.ALIGN_CENTER);
		document.add(loginTextPre);
		document.newPage();
		
		driver.findElement(loginText).sendKeys(numCliente);
		
		captura.takeScreenShotTest(driver, "loginText");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 3: Ingresar No. De Codigo cliente a 8 digitos", fuente));
		
		Image loginText = Image.getInstance("screen\\loginText.png");
		loginText.scaleAbsoluteWidth(260f);
		loginText.scaleAbsoluteHeight(540f);
		loginText.setAlignment(Element.ALIGN_CENTER);
		document.add(loginText);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(slideButton).click();
		captura.takeScreenShotTest(driver, "slideButton");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 4: Dar clic en el check Recordar Usuario y dar clic en boton ingresar", fuente));
		Image slideButton = Image.getInstance("screen\\slideButton.png");
		slideButton.scaleAbsoluteWidth(260f);
		slideButton.scaleAbsoluteHeight(540f);
		slideButton.setAlignment(Element.ALIGN_CENTER);
		document.add(slideButton);
		document.newPage();
		helper.sleepSeconds(2);
		
		document.add(pdf.encabezado());
		driver.findElement(ingresarButton).click();
		captura.takeScreenShotTest(driver, "ingresarButton");
		helper.sleepSeconds(2);
		System.out.println(passwordI);
		driver.findElement(passwordText).click();
		driver.findElement(passwordText).sendKeys(passwordI);
		captura.takeScreenShotTest(driver, "passwordText");
		document.add(new Paragraph("Paso 5: Igresar Contrase�a de Usuario y dar clic en el boton Continuar", fuente));
		Image passwordText = Image.getInstance("screen\\passwordText.png");
		passwordText.scaleAbsoluteWidth(260f);
		passwordText.scaleAbsoluteHeight(540f);
		passwordText.setAlignment(Element.ALIGN_CENTER);
		document.add(passwordText);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(ingButton).click();
		captura.takeScreenShotTest(driver, "ingButton");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Mostrar la pantalla de Personaliza tu Acceso", fuente));
		Image ingButton = Image.getInstance("screen\\ingButton.png");
		ingButton.scaleAbsoluteWidth(260f);
		ingButton.scaleAbsoluteHeight(540f);
		ingButton.setAlignment(Element.ALIGN_CENTER);
		document.add(ingButton);
		document.newPage();
		helper.sleepSeconds(2);	
		
		driver.findElement(imgSlideButton).click();
		captura.takeScreenShotTest(driver, "imgSlideButton");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 6: Seleccionar imagen del carrusel", fuente));
		Image imgSlideButton = Image.getInstance("screen\\imgSlideButton.png");
		imgSlideButton.scaleAbsoluteWidth(260f);
		imgSlideButton.scaleAbsoluteHeight(540f);
		imgSlideButton.setAlignment(Element.ALIGN_CENTER);
		document.add(imgSlideButton);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(fraseText).sendKeys(frase);
		captura.takeScreenShotTest(driver, "fraseText");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 7: Ingresar Frase de bienvenida.", fuente));
		Image fraseText = Image.getInstance("screen\\fraseText.png");
		fraseText.scaleAbsoluteWidth(260f);
		fraseText.scaleAbsoluteHeight(540f);
		fraseText.setAlignment(Element.ALIGN_CENTER);
		document.add(fraseText);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(selecButton).click();
		captura.takeScreenShotTest(driver, "selecButton");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 8: Mostrar opciones para Pregunta Secreta", fuente));
		Image selecButton = Image.getInstance("screen\\selecButton.png");
		selecButton.scaleAbsoluteWidth(260f);
		selecButton.scaleAbsoluteHeight(540f);
		selecButton.setAlignment(Element.ALIGN_CENTER);
		document.add(selecButton);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(selectPregunta).click();
		captura.takeScreenShotTest(driver, "selectPregunta");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 9: Seleccionar una pregunta secreta, que se muestra de la lista desplegable", fuente));
		document.add(new Paragraph("Dar clic en continuar.", fuente));
		Image selectPregunta = Image.getInstance("screen\\selectPregunta.png");
		selectPregunta.scaleAbsoluteWidth(260f);
		selectPregunta.scaleAbsoluteHeight(540f);
		selectPregunta.setAlignment(Element.ALIGN_CENTER);
		document.add(selectPregunta);
		document.newPage();
		helper.sleepSeconds(2);
		
		document.add(pdf.encabezado());
		driver.findElement(continuarButton).click();
		captura.takeScreenShotTest(driver, "continuarButton");
		Image continuarButton = Image.getInstance("screen\\continuarButton.png");
		continuarButton.scaleAbsoluteWidth(260f);
		continuarButton.scaleAbsoluteHeight(540f);
		continuarButton.setAlignment(Element.ALIGN_CENTER);
		document.add(continuarButton);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(respuesta).sendKeys(respuestab);
		captura.takeScreenShotTest(driver, "respuesta");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Paso 10: Ingresar Respuesta Secreta ", fuente));
		Image respuesta = Image.getInstance("screen\\respuesta.png");
		respuesta.scaleAbsoluteWidth(260f);
		respuesta.scaleAbsoluteHeight(540f);
		respuesta.setAlignment(Element.ALIGN_CENTER);
		document.add(respuesta);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(slideRecordar).click();
		captura.takeScreenShotTest(driver, "slideRecordar");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Activar opci�n Recordar Usuario", fuente));
		document.add(new Paragraph("Clic en el boton Continuar", fuente));
		Image slideRecordar = Image.getInstance("screen\\slideRecordar.png");
		slideRecordar.scaleAbsoluteWidth(260f);
		slideRecordar.scaleAbsoluteHeight(540f);
		slideRecordar.setAlignment(Element.ALIGN_CENTER);
		document.add(slideRecordar);
		document.newPage();
		helper.sleepSeconds(2);
		
		document.add(pdf.encabezado());
		driver.findElement(continuarb).click();
		captura.takeScreenShotTest(driver, "continuarb");
		Image continuarb = Image.getInstance("screen\\continuarb.png");
		continuarb.scaleAbsoluteWidth(260f);
		continuarb.scaleAbsoluteHeight(540f);
		continuarb.setAlignment(Element.ALIGN_CENTER);
		document.add(continuarb);
		document.newPage();
		helper.sleepSeconds(2);
		
		document.add(pdf.encabezado());
		driver.findElement(continuarf).click();
		js.executeScript("document.body.style.zoom='.8'");
		System.out.println("Aqui se esta haciendo el zoom");
		captura.takeScreenShotTest(driver, "continuarf");
		Image continuarf = Image.getInstance("screen\\continuarf.png");
		continuarf.scaleAbsoluteWidth(260f);
		continuarf.scaleAbsoluteHeight(540f);
		continuarf.setAlignment(Element.ALIGN_CENTER);
		document.add(continuarf);
		document.newPage();
		document.add(pdf.encabezado());
		document.add(new Paragraph("Mostrar pantalla de 'MIS PRODUCTOS'", fuente));
		document.add(new Paragraph("Muestra Saldos y Movimientos", fuente));
		captura.takeScreenShotTest(driver, "finc");
		Image finc = Image.getInstance("screen\\finc.png");
		finc.scaleAbsoluteWidth(260f);
		finc.scaleAbsoluteHeight(540f);
		finc.setAlignment(Element.ALIGN_CENTER);
		document.add(finc);
		document.newPage();
		helper.sleepSeconds(2);
		
		driver.findElement(detallet).click();
		captura.takeScreenShotTest(driver, "detallet");
		document.add(pdf.encabezado());
		document.add(new Paragraph("Mostrar Pantalla Detalle de Movimientos", fuente));
		Image detallet = Image.getInstance("screen\\detallet.png");
		detallet.scaleAbsoluteWidth(260f);
		detallet.scaleAbsoluteHeight(540f);
		detallet.setAlignment(Element.ALIGN_CENTER);
		document.add(detallet);
		document.newPage();
		helper.sleepSeconds(2);	
		
		
	
	}catch (Exception  ex){
		System.out.println("Existe algun error!! ojo!");
		System.out.println(ex);
		document.add(new Paragraph("Existe algun error!!", fuente));
	}
		document.close();
	}
	
}
