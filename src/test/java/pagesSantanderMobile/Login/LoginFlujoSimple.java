package pagesSantanderMobile.Login;

import java.io.FileOutputStream;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import testSantanderMobile.DriverTest;
import testSantanderMobile.TestLogin;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import ElementsSantaderMobile.ElementsLogin;
import ElementsSantaderMobile.Rutas;
import EventosSantanderMobile.Eventos;
import helpers.Helpers;
import helpers.PdfGeneration;
import helpers.ScreenShots;
import items.Fecha;
import items.Fuentes;

public class LoginFlujoSimple {
	Helpers helper = new Helpers();
	ElementsLogin elements = new ElementsLogin();
	PdfGeneration pdf = new PdfGeneration();
	ScreenShots captura = new ScreenShots();
	Document document = new Document();
	java.util.Date fecha = new Date();
	Rutas rutas = new Rutas();
	Fecha nameFecha = new Fecha();
	Fuentes estilos = new Fuentes();
	DriverTest testDriver = new DriverTest();
	Eventos eventos = new Eventos();
	
	private WebDriver driver;
	private By loginButton;
	private By loginText;
	private By ingresarButton;
	private By passwordText;
    private By slideButton;
	private By ingButton;
	private By continuarf;

	
	public LoginFlujoSimple(WebDriver driver) {
		this.driver = driver;
		loginButton = By.xpath(elements.BTN_INICIO);
		loginText   = By.id(elements.INPUT_CCLIENTE);
		ingresarButton = By.xpath(elements.BTN_INGRESAR);
		passwordText = By.id(elements.PASSWORD);
		slideButton = By.id(elements.BTN_SLIDE);
		ingButton = By.xpath(elements.BTN_ING);
		continuarf = By.xpath(elements.CONTINUAR_F);

	}
	
	public void loginSimple(String numCliente, String passwordI, String frase, String respuestab) throws Exception {    			
		testDriver.inyecta(driver);

		
		pdf.instancia();
		pdf.open();
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("LOGIN: CODIGO DE CLIENTE ");
		pdf.parrafo("**Ingresar los datos de enrolamiento exitosamente y pasar a la pantalla de configuracion exitosa, La cual permitir� continuar con el proceso de registro del cliente.");
		pdf.nuevaPagina(); 
		
		pdf.agregaImg(pdf.encabezado());
		captura.takeScreenShotTest(driver, "loginButton");
		eventos.clicXpath(eventos, elements.BTN_INICIO, driver);
		pdf.parrafo("Paso 1: Abrir super movil.");
		pdf.parrafo("Paso 2: Dar clic en el boton Ingresar");
		pdf.agregaImg(pdf.imageEvidencia("loginButton"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);
		
		
		eventos.textId(eventos, elements.PASSWORD, passwordI, driver);
		captura.takeScreenShotTest(driver, "loginText");
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("Paso 3: Ingresar No. De Codigo cliente a 8 digitos");
		pdf.agregaImg(pdf.imageEvidencia("loginText"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);

		driver.findElement(slideButton).click();
		captura.takeScreenShotTest(driver, "slideButton");
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("Paso 3: Ingresar No. De Codigo cliente a 8 digitos");
		pdf.parrafo("Paso 4: Dar clic en el check Recordar Usuario y dar clic en boton ingresar");
		pdf.agregaImg(pdf.imageEvidencia("slideButton"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);
		
		pdf.agregaImg(pdf.encabezado());
		driver.findElement(ingresarButton).click();
		captura.takeScreenShotTest(driver, "ingresarButton");
		helper.sleepSeconds(2);
		driver.findElement(passwordText).sendKeys(passwordI);
		captura.takeScreenShotTest(driver, "passwordText");
		pdf.parrafo("Igresar Contrase�a de Usuario.");
		pdf.agregaImg(pdf.imageEvidencia("passwordText"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);
		
		driver.findElement(ingButton).click();
		captura.takeScreenShotTest(driver, "ingButton");
		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("Mostrar la pantalla de Mis productos");
		pdf.agregaImg(pdf.imageEvidencia("ingButton"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);	

		pdf.agregaImg(pdf.encabezado());
		pdf.parrafo("Mostrar pantalla de 'MIS PRODUCTOS'");
		pdf.parrafo("Muestra Saldos y Movimientos");
		captura.takeScreenShotTest(driver, "finc");
		pdf.agregaImg(pdf.imageEvidencia("finc"));
		pdf.nuevaPagina();
		helper.sleepSeconds(2);
		
		pdf.close();
	}
	
}
