package testSantanderMobile;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.SessionStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.html5.RemoteLocalStorage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ElementsSantaderMobile.Rutas;
import helpers.Helpers;
import pagesSantanderMobile.Login.LoginFlujoSimple;
import pagesSantanderMobile.Login.LoginRSA;
import pagesSantanderMobile.Login.LoginRecordarUsuario;

public class TestLogin {
	private WebDriver driver;
	Helpers helper = new Helpers();
	WebDriver webDriver = null;
	Rutas url = new Rutas();

	@BeforeMethod
	public void setUp() {
		
		DesiredCapabilities caps = new DesiredCapabilities();
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
		
		//Con este bloque dimensionamos pero solo es el tama�o de la ventana no es responsivo
		driver = new ChromeDriver();	
		Dimension d = new Dimension(300,680);
		driver.manage().window().setSize(d);	
		driver.navigate().to(url.AMBIENTE);
//__________________________________________________________________________________________

		//Vista Responsive con dispositivos especificos.
//		Map<String, Object> deviceMetrics = new HashMap<>();
//		deviceMetrics.put("width", 375);
//		deviceMetrics.put("height", 812);
//		//deviceMetrics.put("pixelRatio", 3.0);
//		Map<String, Object> mobileEmulation = new HashMap<>();
//		mobileEmulation.put("deviceMetrics", deviceMetrics);
//		mobileEmulation.put("userAgent",
//				"Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
//		ChromeOptions chromeOptions = new ChromeOptions();
//		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
//		driver = new ChromeDriver(chromeOptions);
//		driver.manage().window().maximize();	
//		driver.navigate().to(url.AMBIENTE);
//___________________________________________________________________________________________
		
		//Lineas que nos sirven en caso de necesitar variables de sesion
//		JavascriptExecutor js = (JavascriptExecutor)driver;  
//		js.executeScript("localStorage.setItem('userId','88888888');"); 
//		js.executeScript("localStorage.setItem('userRSA',' {\"phrase\":\"hii\", \"questionId\":\"Q3.1\", \"response\":\"london\", \"imagePath\":\"000000623291RS.jpg\"} ');"); 
//___________________________________________________________________________________________
		
		
		helper.sleepSeconds(3);
	}

	@Test
	public void pruebaUnoLogin(String numCliente, String passwordI, String frase, String respuesta) throws Exception {
		LoginRSA login = new LoginRSA(driver);
		login.loginUno(numCliente, passwordI, frase, respuesta);
	}
	
	@Test
	public void pruebaDosLogin(String numCliente, String passwordI, String frase, String respuesta) throws Exception {
		LoginFlujoSimple login = new LoginFlujoSimple(driver);
		login.loginSimple(numCliente, passwordI, frase, respuesta);
	}
	
	@Test
	public void pruebaTresLogin(String numCliente, String passwordI, String frase, String respuesta) throws Exception {
		LoginRecordarUsuario login = new LoginRecordarUsuario(driver);
		login.loginDos(numCliente, passwordI, frase, respuesta);
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}

}
