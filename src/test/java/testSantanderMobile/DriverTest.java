package testSantanderMobile;

import helpers.Helpers;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pagesSantanderMobile.Login.Clarifications;
import pagesSantanderMobile.Login.LoginFlujoSimple;
import pagesSantanderMobile.Login.LoginRSA;
import pagesSantanderMobile.Login.LoginRecordarUsuario;
import pagesSantanderMobile.Login.classFlujoIDP;

import ElementsSantaderMobile.Rutas;

public class DriverTest {
	private WebDriver driver;
	Helpers helper = new Helpers();
	WebDriver webDriver = null;
	Rutas url = new Rutas();
	
	@BeforeMethod
	public WebDriver setUp() {

		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver_80.exe");
		driver = new ChromeDriver();	
		Dimension d = new Dimension(300,680);
//		driver.manage().window().setSize(d);
		driver.manage().window().maximize();
		driver.navigate().to(url.AMBIENTE);
		helper.sleepSeconds(3);
		return driver;
	}
	
	
	//Setea informacion al localstorage para simular acciones 
	public void inyecta(WebDriver drivercall){
		System.out.println("driver 3 " + drivercall);
		JavascriptExecutor js = (JavascriptExecutor)drivercall;  
		js.executeScript("localStorage.setItem('userId','88888888');"); 
		js.executeScript("localStorage.setItem('userRSA',' {\"phrase\":\"hii\", \"questionId\":\"Q3.1\", \"response\":\"london\", \"imagePath\":\"000000623291RS.jpg\"} ');");
	}
	
	//Hace Scroll sobre la pantalla
	public void scoll(WebDriver driverScroll){
		JavascriptExecutor js = (JavascriptExecutor)driverScroll; 
		js.executeScript("window.scrollBy(0,700)");
	}
	@Test
	public void pruebaUnoLogin(String numCliente, String passwordI, String frase, String respuesta) throws Exception {
		LoginRSA login = new LoginRSA(driver);
		login.loginUno(numCliente, passwordI, frase, respuesta);
	}
	
	@Test
	public void pruebaDosLogin(String numCliente, String passwordI, String frase, String respuesta) throws Exception {
		LoginFlujoSimple login = new LoginFlujoSimple(driver);
		login.loginSimple(numCliente, passwordI, frase, respuesta);
	}
	
	@Test
	public void pruebaTresLogin(String numCliente, String passwordI, String frase, String respuesta) throws Exception {
		LoginRecordarUsuario login = new LoginRecordarUsuario(driver);
		login.loginDos(numCliente, passwordI, frase, respuesta);
	}
	
	@Test
	public void driverFlujoIdp (String numCliente, String password) throws Exception {
		classFlujoIDP idp = new classFlujoIDP(driver);
		idp.classFlujoIDP(numCliente, password);
	}
	
	@Test
	public void clarificationsFlow () throws Exception {
		Clarifications idp = new Clarifications(driver);
		idp.clarificationsMov();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}


	}	


