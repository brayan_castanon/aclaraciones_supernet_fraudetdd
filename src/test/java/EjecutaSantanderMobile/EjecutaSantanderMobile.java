package EjecutaSantanderMobile;


import java.util.List;

import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.remote.html5.RemoteLocalStorage;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;

import ElementsSantaderMobile.Rutas;
import helpers.Helpers;
import helpers.ObtieneRegistro;
import helpers.PdfGeneration;
import helpers.Registro;
import testSantanderMobile.DriverTest;
import testSantanderMobile.TestLogin;

public class EjecutaSantanderMobile {
	
	public static void loginRSA () throws Exception {
		Helpers helper = new Helpers();
		ObtieneRegistro or = new ObtieneRegistro();
		DriverTest testerDr = new DriverTest();
		
		Rutas url = new Rutas ();

		List<Registro> registros = or.obtenerRegistros(url.PULL_DATOS);

		// Flujo de Login
		for (Registro r : registros) {
			testerDr.setUp();
			helper.sleepSeconds(2);
			testerDr.pruebaUnoLogin(r.getNoCliente(), r.getPassword(), r.getFraseB(), r.getRespuestab());

			testerDr.tearDown();
		}
	}
	
	public static void loginSimple () throws Exception {
		Helpers helper = new Helpers();
		ObtieneRegistro or = new ObtieneRegistro();
		Rutas url = new Rutas ();
		DriverTest testerDr = new DriverTest();

		List<Registro> registros = or.obtenerRegistros(url.PULL_DATOS);

		// Flujo de Login
		for (Registro r : registros) {
			testerDr.setUp();
			helper.sleepSeconds(2);
			testerDr.pruebaDosLogin(r.getNoCliente(), r.getPassword(), r.getFraseB(), r.getRespuestab());

			testerDr.tearDown();
		}
		
	}
	
	public static void loginRecordarU () throws Exception {
		Helpers helper = new Helpers();
		ObtieneRegistro or = new ObtieneRegistro();
		Rutas url = new Rutas ();
		DriverTest testerDr = new DriverTest();

		List<Registro> registros = or.obtenerRegistros(url.PULL_DATOS);

		// Flujo de Login
		for (Registro r : registros) {
			testerDr.setUp();
			helper.sleepSeconds(2);
			testerDr.pruebaTresLogin(r.getNoCliente(), r.getPassword(), r.getFraseB(), r.getRespuestab());

			testerDr.tearDown();
		}
		
	}
	
	public static void flujoIdp () throws Exception {
		Helpers helper = new Helpers();
		ObtieneRegistro or = new ObtieneRegistro();
		Rutas url = new Rutas ();
		DriverTest testerDr = new DriverTest();

		List<Registro> registros = or.obtenerRegistros(url.PULL_DATOS);

		// Flujo de Login
		for (Registro r : registros) {
			testerDr.setUp();
			helper.sleepSeconds(2);
			testerDr.driverFlujoIdp(r.getNoCliente(), r.getPassword());

			testerDr.tearDown();
		}
		
	}
	
	public static void clarifications () throws Exception {
		Helpers helper = new Helpers();
		ObtieneRegistro or = new ObtieneRegistro();
		Rutas url = new Rutas ();
		DriverTest testerDr = new DriverTest();

		List<Registro> registros = or.obtenerRegistros(url.PULL_DATOS);

		// Flujo de Login
		for (Registro r : registros) {
			testerDr.setUp();
			helper.sleepSeconds(2);
			testerDr.clarificationsFlow();

			testerDr.tearDown();
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		DriverTest test1 = new DriverTest();
//		loginRSA();
//		System.out.println("Se corrio el flujo RSA");
//		loginSimple ();
//		System.out.println("Se corrio el flujo simple");
//		loginRecordarU ();
//		System.out.println("Se corrio el flujo recordar usuario");
		clarifications();
		
		
		
	}
}
