package ElementsSantaderMobile;

public class ElementsLogin {
	public ElementsLogin () {
	}
	
	/**
	 * Click para ingresar
	 */
	public static final String BTN_INICIO = "(.//*[normalize-space(text()) and normalize-space(.)='Santander M�vil!'])[1]/following::button[1]";
											  
	
	/**
	 * Codigo de cliente
	 */
	public static final String INPUT_CCLIENTE = "clientCode";
	
	/**
	 * Click para continuar 
	 */
	public static final String BTN_INGRESAR = "(.//*[normalize-space(text()) and normalize-space(.)='Recordar usuario'])[1]/following::button[1]";
											   
										
	/**
	 * Ingresa password.
	 */
	public static final String PASSWORD = "password";
	
	/**
	 * Recordar usuario
    */
	public static final String BTN_SLIDE = "button-1";

	/**
	 * Click para iniciar sesi�n
	 */
	public static final String BTN_ING = "(.//*[normalize-space(text()) and normalize-space(.)='Contrase�a de usuario'])[1]/following::button[1]";
	
	/**
	 * Seleccionar imagen
    */
	public static final String BTN_SLIDE_IMG = "(.//*[normalize-space(text()) and normalize-space(.)='Ingresa tu contrase�a de usuario'])[1]/following::i[2]";
	/**
	 * Ingresar Frase de Bienvenida
    */
	public static final String FRASE_BIENV = "creditCardNumber";
	
	/**
	 * Mostrar men� de pregunta secreta
    */
	public static final String PREGUNTA_SECRETA = "(.//*[normalize-space(text()) and normalize-space(.)='Selecciona una pregunta secreta'])[1]/following::input[1]";
	
	/**
	 * Seleccionar pregunta secreta
    */
	public static final String SELEC_SECRETA = "(.//*[normalize-space(text()) and normalize-space(.)='Selecciona una opci�n:'])[1]/following::div[6]";
	
	/**
	 * Clic continuar
    */
	public static final String CONTINUAR = "strech";
		
	/**
	 * Ingresar respuesta de pregunta secreta
    */
	public static final String RESPUESTA = "(.//*[normalize-space(text()) and normalize-space(.)='Indica tu respuesta'])[1]/following::input[1]";
	
	/**
	 * Activar el boton recordar el usuario
    */
	public static final String SLIDE_RECORDAR = "button-2";
	
	/**
	 * Clic continuar 
    */
	public static final String CONTINUAR_B = "(.//*[normalize-space(text()) and normalize-space(.)='Recordar usuario'])[1]/following::button[1]";
	
	/**
	 * Clic continuar 
    */
	public static final String CONTINUAR_F = "(.//*[normalize-space(text()) and normalize-space(.)='A partir de este momento ya puedes acceder a la Banca Digital Santander'])[1]/following::button[1]";
	
	/**
	 * Clic para mostrar detalle de una cuenta
    */
	public static final String DETALLE_T = "(.//*[normalize-space(text()) and normalize-space(.)='MXP'])[2]/following::p[1]";
	
	/**
	 * Clic continuar 
    */
	public static final String CLICK_NAV = "(.//*[normalize-space(text()) and normalize-space(.)='Deposito En Efectivo'])[1]/following::div[2]";
	
	
	public static final String INGRESAR_RECORDAR = "(.//*[normalize-space(text()) and normalize-space(.)='�No eres t�?'])[1]/following::button[1]";	
	

	
	// Aclaraciones
	
	/**
	 * Click en Ver Mas
	 */
	public static final String BTN_VERMAS = "button0";

	/**
	 * Click en el Movimiento
	 */
	public static final String MOVE = "description-move-382";
	
	/**
	 * Click Continuar
	 */
	public static final String CONTINUAR_A = "page-welcome-tdd";
	
	/**
	 * Click Si
	 */
	public static final String YES = "[for='YesHasCard']";
	
	/**
	 * Click No
	 */
	public static final String NOT = "[for='NoInteraction']";
	
	/**
	 * Click Nacional
	 */
	public static final String NATIONAL = "[for='InMexico']";
	
	/**
	 * Click Estado
	 */
	public static final String STATE = "select#state > option[value='"+"6"+"']";
	
	/**
	 * Click Continuar
	 */
	public static final String CONTINUAR_Q = "page-questionnaire-tdd";
	
	/**
	 * Click Confirmar
	 */
	public static final String CONFIRMAR_SUMARY = "page-summary-tdd";
	
	
	/**
	 * Click Confirmar Modal
	 */
	public static final String COFIRMAR_MODAL = "finish";
	
	/**
	 * Click Reposicion Inmediata
	 */
	public static final String RADIOINM = "[for='description-express']";
	
	/**
	 * Click Continuar Summary
	 */
	public static final String CONTINUAR_SUMMARY2 = "page-locked-tdd";
	

	

	
	
	
	







}
