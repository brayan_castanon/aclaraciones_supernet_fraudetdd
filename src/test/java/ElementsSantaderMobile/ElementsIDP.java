package ElementsSantaderMobile;

public class ElementsIDP {
	public ElementsIDP () {
	}
	
	/**
	 * Click para ingresar
	 */
	public static final String BOX_CODCL= "userId";
											  
	
	/**
	 * Codigo de cliente
	 */
	public static final String BTN_CONTINUAR = "btnSendUser";
	
	/**
	 * Click para continuar 
	 */
	public static final String BOX_PASSWORD = "userPassword";
											   
										
	/**
	 * Ingresa password.
	 */
	public static final String BTN_PASSWORD = "btnSendPassword";
	
	/**
	 * Ingresa password.
	 */
	public static final String ATENCION = "modal-header";


}
