package ElementsSantaderMobile;

public class ElementsClarification {
	public ElementsClarification () {
	}
	

	// Aclaraciones
	
	/**
	 * Click en Ver Mas
	 */
	public static final String BTN_VERMAS = "button0";

	/**
	 * Click en el Movimiento
	 */
	public static final String MOVE = "description-move-382";
	
	/**
	 * Click Continuar
	 */
	public static final String CONTINUAR_A = "page-welcome-tdd";
	
	/**
	 * Click Si
	 */
	public static final String YES = "[for='YesHasCard']";
	
	/**
	 * Click No
	 */
	public static final String NOT = "[for='NoInteraction']";
	
	/**
	 * Click Nacional
	 */
	public static final String NATIONAL = "[for='InMexico']";
	
	/**
	 * Click Estado
	 */
	public static final String STATE = "select#state > option[value='"+"6"+"']";
	
	/**
	 * Click Continuar
	 */
	public static final String CONTINUAR_Q = "page-questionnaire-tdd";
	
	/**
	 * Click Confirmar
	 */
	public static final String CONFIRMAR_SUMARY = "page-summary-tdd";
	
	
	/**
	 * Click Confirmar Modal
	 */
	public static final String COFIRMAR_MODAL = "finish";
	
	/**
	 * Click Reposicion Inmediata
	 */
	public static final String RADIOINM = "[for='description-express']";
	
	/**
	 * Click Continuar Summary
	 */
	public static final String CONTINUAR_SUMMARY2 = "page-locked-tdd";
	

	

	
	
	
	







}
