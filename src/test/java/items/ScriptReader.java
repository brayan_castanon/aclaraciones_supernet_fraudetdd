package items;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
 
/**
 * @author ManuelFrancisco
 */
public class ScriptReader {
 
    /**
     * Archivo JavaScript.
     */
    private static final File SCRIPT = new File("C:\\Users\\Santander\\eclipse-workspace\\TestSantanderMobile\\js\\localstorage.js");
    /**
     * Motor de ejecuci&oacute;n JavaScript.
     */
    private static final ScriptEngine ENGINE = new ScriptEngineManager().
        getEngineByName("javascript");
 
    public static void main(String[] args) {
        try {
            // Leemos el archivo JavaScript.
            FileReader fr = new FileReader(SCRIPT);
             
            // Ejecutamos el JavaScript
            Object result = ENGINE.eval(fr);
 
            // Imprimimos el retorno.
            System.out.println(result);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ScriptException ex) {
            ex.printStackTrace();
        }
    }
}