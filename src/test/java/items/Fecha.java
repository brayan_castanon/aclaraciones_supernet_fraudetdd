package items;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Fecha {
	public String name () {
		Date fechaHoy = new Date();
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddHHmmss");
	    String namefecha = formato.format(fechaHoy);
	    return namefecha;
	}
}
