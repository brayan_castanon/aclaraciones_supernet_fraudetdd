package helpers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.*;
import jxl.read.biff.BiffException;

/**
 * 
 * @author Z058228
 *
 * Clase que obtiene insumos de una hoja de calculo.
 *
 */
public class Obtiene_Insumos {
	
	/**
	 * 
	 * @param file (ruta de hoja de calculo .xls)
	 * @return String[][]
	 * 
	 * Metodo que obtiene un arreglo de dos dimensiones a partir de una hoja de calculo.
	 */

	public String[][] getInsumos(String file){
		
		String [][] matriz = null;
		
		List<List<String>> listas = this.getListaInsumosCol(file);
		
		matriz = new String [listas.size()][listas.get(0).size()];
		
		int i = 0;
		for(List<String> datos : listas) {
			int j = 0;
			for(String dato: datos) {
				matriz[i][j] = dato;
				j++;
			}
			i++;
		}		
		return matriz;		
	}//Fin getInsumos
	
	
	/**
	 * 
	 * @param file (ruta de hoja de calculo .xls)
	 * @return List<List<String>>
	 * 
	 * Metodo que obtiene una lista de listas de tipo cadena a partir de una hoja de calculo.
	 * 
	 */
	public List<List<String>> getListaInsumosCol(String file){
	
		List<List<String>> listas = new ArrayList<List<String>> ();
		
		try {
		
			Workbook workbook = Workbook.getWorkbook(new File(file));
			Sheet sheet = workbook.getSheet(0);//Hoja que se va a leer
			
			for (int columna = 0; columna < sheet.getColumns(); columna++) { //recorremos las columnas
				listas.add(new ArrayList<String> ());
				System.out.println("Columnas " + columna);
				
				for (int fila = 0; fila < sheet.getRows(); fila++) { //recorremos las filas
					listas.get(columna).add(sheet.getCell(columna, fila).getContents());
					System.out.println("Filas " + fila);

				}
			}
		
		}catch(Exception  e) {
			e.printStackTrace();
		}
		return listas;	
	}//Fin getListaInsumos
	
	

	/**
	 * 
	 * @param file (ruta de hoja de calculo .xls)
	 * @return List<List<String>>
	 * 
	 * Metodo que obtiene una lista de listas de tipo cadena a partir de una hoja de calculo.
	 * 
	 */
	public List<List<String>> getListaInsumosFila(String file){
	
		List<List<String>> listas = new ArrayList<List<String>> ();
		
		try {
		
			Workbook workbook = Workbook.getWorkbook(new File(file));
			Sheet sheet = workbook.getSheet(0);//Hoja que se va a leer
			
			for (int fila = 0; fila < sheet.getRows(); fila++) { //recorremos las filas
				listas.add(new ArrayList<String> ());
				
				for (int columna = 0; columna < sheet.getColumns(); columna++) { //recorremos las columnas
					listas.get(fila).add(sheet.getCell(columna, fila).getContents());
				}
			}
		
		}catch( Exception   e) {
			e.printStackTrace();
		}
		return listas;	
	}//Fin getListaInsumos
	
	
//	public static void main(String[] args) {
//		
//		Obtiene_Insumos oi = new Obtiene_Insumos();
//		List<List<String>> out = oi.getListaInsumosCol("C:\\Users\\Z060612.GFSSCORP\\Desktop\\testAutomation\\TestSantanderMobile\\pull\\DatosValidos2.xls");
//		
//		System.out.println("Tama�o de la lista: " + out.size());
//		
//		for(List<String> datos : out) {
//			System.out.println("Tama�o de la sublista: " + datos.size());
//			for(String dato: datos) {
//				System.out.println("El dato es: " + dato);
//			}
//		}
//		oi.getInsumos("C:\\Users\\Z060612.GFSSCORP\\Desktop\\testAutomation\\TestSantanderMobile\\pull\\DatosValidos2.xls");
//		
//		List<List<String>> out2 = oi.getListaInsumosFila("C:\\Users\\Z060612.GFSSCORP\\Desktop\\testAutomation\\TestSantanderMobile\\pull\\DatosValidos2.xls");
//		
//		for(List<String> datos : out2) {
//			System.out.println("Tama�o de la sublista: " + datos.size());
//			for(String dato: datos) {
//				System.out.println("El dato es: " + dato);
//			}
//		}
//		
//	}
	
	
	
}
