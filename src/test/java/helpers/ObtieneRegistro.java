package helpers;
import java.util.ArrayList;
import java.util.List;


public class ObtieneRegistro {

	/**
	 * Metodo que almacena los datos de una hoja de calculo en objetos tipo registro.	
	 * @param file
	 * @return List<Registro>
	 */
	public List<Registro> obtenerRegistros(String file){
		List<Registro> registros = null;
		try {
			Obtiene_Insumos oi = new Obtiene_Insumos();
			List<List<String>> listas = oi.getListaInsumosFila(file);
			
			Registro registro = new Registro();
			registros = new ArrayList<Registro>();
			
			for(List<String> lista: listas) {
				//Agregar aqui los elementos tipo texto del DOM
				registro.setNoCliente(lista.get(0));
				registro.setPassword(lista.get(1));
//				registro.setFraseB(lista.get(2));
//				registro.setRespuestab(lista.get(3));
				
				registros.add(registro);
				
				registro = new Registro();
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return registros;	
	}
	
//	public static void main(String[] args) {
//		
//		ObtieneRegistro or = new ObtieneRegistro();
//		
//		List<Registro> registros = or.obtenerRegistros("C:\\Users\\Z060612.GFSSCORP\\Desktop\\testAutomation\\TestSantanderMobile\\pull\\DatosValidos2.xls");
//		
//		int cont = 1;
//		for(Registro r : registros) {
//			System.out.println("Registro "+cont+": " );
//			System.out.println("NoCliente: "+r.getNoCliente());
//			System.out.println("Password: "+r.getPassword());
//			cont++;
//		}
//		
//	}
}
