package helpers;

import items.Fecha;
import items.Fuentes;

import java.io.FileOutputStream;
import java.util.Date;

import org.openqa.selenium.WebDriver;

import testSantanderMobile.DriverTest;

import ElementsSantaderMobile.ElementsLogin;
import ElementsSantaderMobile.Rutas;
import EventosSantanderMobile.Eventos;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;


public class PdfGeneration {
	Helpers helper = new Helpers();
	ElementsLogin elements = new ElementsLogin();
	ScreenShots captura = new ScreenShots();
	Document document = new Document();
	java.util.Date fecha = new Date();
	Rutas rutas = new Rutas();
	Fecha nameFecha = new Fecha();
	Fuentes estilos = new Fuentes();
	DriverTest testDriver = new DriverTest();
	Eventos eventos = new Eventos();
	int cont = 0;
	
	Font  fuente = estilos.styles();
	String File = rutas.EVIDENCIAS + nameFecha.name() + ".pdf";
	

	public void instancia () throws Exception{
		PdfWriter.getInstance(document, new FileOutputStream(File));
	}
	
	public void open () throws Exception{

		document.open();
		System.out.println("Abriendo documento");
		
	}
	
	public void status (){
		Boolean abierto = document.isOpen();
		System.out.println(abierto);
	}
		
	public  Image encabezado () throws Exception {
		Image cabecera = Image.getInstance("encabezado\\cabecera.png");
		cabecera.scaleAbsoluteWidth(510f);
		cabecera.scaleAbsoluteHeight(130f);
		cabecera.setAlignment(Element.ALIGN_CENTER);
		return cabecera;	
	}

	public void parrafo (String texto)throws Exception {
		document.add(new Paragraph(texto, fuente ));	
	}
	
	public void agregaImg (Image imagen) throws Exception {
		System.out.println("aqui esta la imagen: "+ imagen);
		document.add(imagen);
		System.out.println("aqui esta la imagen 2: "+ imagen);
	}
	
	public Image imageEvidencia (String name) throws Exception{
		Image captura = Image.getInstance("Report\\Screen\\"+ name + ".png");
		System.out.println("aqui esta la imagen 2: "+ captura);
		captura.scaleAbsoluteWidth(560f);
		captura.scaleAbsoluteHeight(340f);
		captura.setAlignment(Element.ALIGN_CENTER);
		
		return captura;
	}
	
	public void nuevaPagina (){
		Boolean abierto = document.isOpen();
		System.out.println("nueva: " + abierto);
		document.newPage();
	}
	
	public void add (WebDriver driver) throws Exception {
		captura.takeScreenShotTest(driver, "IMAGEN00" );
		agregaImg(imageEvidencia("IMAGEN00"));
	}
	
	public void close (){
		System.out.println("cerrando");
		document.close();
	}

}
