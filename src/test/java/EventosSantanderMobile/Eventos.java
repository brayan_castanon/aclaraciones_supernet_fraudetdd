package EventosSantanderMobile;

import java.util.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import helpers.Helpers;


public class Eventos {
	private WebElement element;
	
	Helpers helper = new Helpers();
	
	public void clicXpath(Eventos eventos, String elemento, WebDriver driver) {
		try {
			element = driver.findElement(By.xpath(elemento));
			element.click();
		} catch (NoSuchElementException nsee) {
			System.out.println("Elemento no encontrado!");
		}
	}
	
	public void clicId(Eventos eventos, String elemento, WebDriver driver) {
		try {
			element = driver.findElement(By.id(elemento));
			element.click();
		} catch (NoSuchElementException nsee) {
			System.out.println("Elemento no encontrado!");
		}
	}
	
	public void clicCss(Eventos eventos, String elemento, WebDriver driver) {
		try {
			element = driver.findElement(By.cssSelector(elemento));
			element.click();
		} catch (NoSuchElementException nsee) {
			System.out.println("Elemento no encontrado!");
		}
	}
	
	public void textId(Eventos eventos, String elemento, String text, WebDriver driver) {
		try {
			element = driver.findElement(By.id(elemento));
			element.sendKeys(text);
		} catch (NoSuchElementException nsee) {
			System.out.println("Elemento no encontrado!");
		}
	}

}
